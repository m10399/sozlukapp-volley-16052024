package com.example.sozlukuygulamasivolleyentegrasyonu

import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request.Method
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.sozlukuygulamasivolleyentegrasyonu.databinding.ActivityMainBinding
import org.json.JSONObject

class MainActivity : AppCompatActivity(),SearchView.OnQueryTextListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var kelimelerListe:ArrayList<Kelimeler>
    private lateinit var adapter:KelimelerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        binding.toolbar.title = "Sözlük Uygulaması"
        setSupportActionBar(binding.toolbar)

        binding.rv.setHasFixedSize(true)
        binding.rv.layoutManager = LinearLayoutManager(this)
        tumKelimeler()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu,menu)
        val item = menu?.findItem(R.id.action_ara)
        val searchView = item?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
       kelimeAra(query!!)
        Log.e("Gönderilen arama",query)
       return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        kelimeAra(newText!!)
        Log.e("Harf girdikçe",newText)
        return true
    }


    fun tumKelimeler(){
        val url = "http://kasimadalan.pe.hu/sozluk/tum_kelimeler.php"
        val istek = StringRequest(Method.GET,url, Listener{ cevap ->
            kelimelerListe = ArrayList()
            try {
                val jsonObject = JSONObject(cevap)
                val kelimeler = jsonObject.getJSONArray("kelimeler")
                for ( i in 0 until kelimeler.length()){
                    val k = kelimeler.getJSONObject(i)
                    val kelime = Kelimeler(k.getInt("kelime_id"),k.getString("ingilizce"), k.getString("turkce"))
                    kelimelerListe.add(kelime)
                }
                adapter = KelimelerAdapter(this@MainActivity,kelimelerListe)
                binding.rv.adapter = adapter
            }catch (e:Exception){
                e.printStackTrace()
            }
        }, {  })
        Volley.newRequestQueue(this).add(istek)
    }
    fun kelimeAra(aramaKelime:String){
        val url = "http://kasimadalan.pe.hu/sozluk/kelime_ara.php"
        val istek = object :  StringRequest(Method.POST,url, Listener{ cevap ->
            kelimelerListe = ArrayList()
            try {
                val jsonObject = JSONObject(cevap)
                val kelimeler = jsonObject.getJSONArray("kelimeler")
                for ( i in 0 until kelimeler.length()){
                    val k = kelimeler.getJSONObject(i)
                    val kelime = Kelimeler(k.getInt("kelime_id"),k.getString("ingilizce"), k.getString("turkce"))
                    kelimelerListe.add(kelime)
                }
                adapter = KelimelerAdapter(this@MainActivity,kelimelerListe)
                binding.rv.adapter = adapter
            }catch (e:Exception){
                e.printStackTrace()
            }
        }, Response.ErrorListener {  }){
            override fun getParams(): MutableMap<String, String> {
                val params = HashMap<String,String>()
                params["ingilizce"] = aramaKelime

                return params
            }
        }
        Volley.newRequestQueue(this).add(istek)
    }
}